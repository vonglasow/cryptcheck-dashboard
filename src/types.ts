export interface Entry {
    site: string;
    cryptcheck: string;
    dns: {
        A: boolean;
        AAAA: boolean;
    };
}
